import string
import random
def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

print(get_random_string(10))