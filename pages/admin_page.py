import string
import random

from selenium.webdriver.common.by import By



class AdminPage:
    admin_button = (By.CSS_SELECTOR, '.header_admin')
    add_project_button = (By.CSS_SELECTOR,'[href="http://demo.testarena.pl/administration/add_project"]')
    name_input = (By.CSS_SELECTOR, '#name')
    prefix_input = (By.CSS_SELECTOR, '#prefix')
    save_button = (By.CSS_SELECTOR, 'span#save')
    info_box = (By.CSS_SELECTOR, '#j_info_box')
    search_input = (By.CSS_SELECTOR, '#search')
    search_button = (By.CSS_SELECTOR, '#j_searchButton')
    projects_name_on_list = (By.CSS_SELECTOR, 'tbody > tr')
    def __init__(self, admin):
        self.admin = admin

    def open_admin_page(self):
        self.admin.find_element(*self.admin_button).click()
        return self

    def open_adding_project_page(self):
        self.admin.find_element(*self.add_project_button).click()
        return self

    def add_project(self, projects_name):
        project_prefix = create_project_prefix()
        self.admin.find_element(*self.name_input).send_keys(projects_name)
        self.admin.find_element(*self.prefix_input).send_keys(project_prefix)
        self.admin.find_element(*self.save_button).click()
        return self

    def get_info_box(self):
        return self.admin.find_element(*self.info_box)

    def seach_project(self, projects_name):
        self.admin.find_element(*self.search_input).send_keys(projects_name)
        self.admin.find_element(*self.search_button).click()

    def get_projects_name_from_projects_list(self):
        return self.admin.find_element(*self.projects_name_on_list).text

def create_project_prefix():
    project_prefix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))
    return project_prefix