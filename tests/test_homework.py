import string
import random

import pytest

from fixtures.chrome import chrome_browser
from fixtures.testarena.login import browser
from fixtures.testarena.login import admin
from pages.admin_page import AdminPage
from pages.home_page import HomePage


def test_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    administrator_email = 'administrator@testarena.pl'
    assert administrator_email == user_email
    assert 'Kokpit - TestArena Demo' in browser.title


def test_open_admin_page(admin):
    assert 'Projekty - TestArena Demo' in admin.title


def test_open_adding_project(admin):
    admin_page = AdminPage(admin)
    admin_page.open_adding_project_page()
    assert 'Dodaj projekt - TestArena Demo' in admin.title


def test_add_project(admin, create_project_name):
    admin_page = AdminPage(admin)
    admin_page.open_adding_project_page().add_project(create_project_name)
    info_box = admin_page.get_info_box()
    assert info_box


def test_search_project(admin, create_project_name):
    admin_page = AdminPage(admin)
    admin_page.seach_project(create_project_name)
    projects_list = admin_page.get_projects_name_from_projects_list()
    assert create_project_name in projects_list

@pytest.fixture(scope="module")
def create_project_name():
    project_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))
    return project_name


